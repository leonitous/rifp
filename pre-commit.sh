#!/bin/bash

# Bash "strict mode", to help catch problems and bugs in the shell
# script. Every bash script you write should include this. See
# http://redsymbol.net/articles/unofficial-bash-strict-mode/ for
# details.
set -euo pipefail

DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"
# echo $DIR

docker pull registry.gitlab.com/leonitous/rifp/opencv:latest

# export DIR=/Users/leo.conforti/Desktop/rifp/master/
docker run --name rifp-testing --volume $DIR/:/rifp/ --interactive --privileged --rm \
    registry.gitlab.com/leonitous/rifp/opencv:latest /bin/bash -c \
    "cd /rifp/; \
    rm -rf build/
    deploy/scripts/extra-cuda-linking.sh; \
    deploy/scripts/setup-all.sh; \
    deploy/scripts/build-all.sh; \
    deploy/scripts/test-all.sh; \
    deploy/scripts/clean-all.sh"

# Windows using linux subsystem for windows (lsw)
# docker run -it --rm --privileged --volume /mnt/c/users/leo/documents/github/rifp/:/rifp/ registry.gitlab.com/leonitous/rifp/opencv:latest

# MacOs x and unix
# docker run -it --rm --privileged --volume /Users/leo.conforti/Desktop/rifp/master/:/rifp/ registry.gitlab.com/leonitous/rifp/opencv:latest
