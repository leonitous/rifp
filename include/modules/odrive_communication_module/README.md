# Odrive communication

c++ library to communicate with the odrive using a python host program and linux FIFO pipes

## Compiling example

```sh
g++ -g -Wall example.cpp -o example -std=c++11 -L/usr/lib/x86_64-linux-gnu/ -lpython3.6m -L/usr/lib/x86_64-linux-gnu/ -lboost_python3 -I/rifp/include/ -I/usr/include/python3.6m -I/usr/include/
```
