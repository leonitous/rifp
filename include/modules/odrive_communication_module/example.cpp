/*
to compile:
g++ -g -Wall example.cpp -o example -std=c++11 -L/usr/lib/x86_64-linux-gnu/ -lpython3.6m -L/usr/lib/x86_64-linux-gnu/ -lboost_python3
-I/rifp/include/ -I/usr/include/python3.6m -I/usr/include/
*/

#include <iostream>

#include "odrive_communication.hpp"

using namespace modules;

int main(int argc, char *argv[])
{
    odrive odrv;
    odrv.execute((char *)"axis0.controller.config.vel_gain", "0.1");

    return 0;
}
