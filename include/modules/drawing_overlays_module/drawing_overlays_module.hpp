/*
C++ functions for adding drawing overlays to opencv images
*/

#include <opencv2/core/mat.hpp>
#include <opencv2/imgproc.hpp>
#include <string>

#pragma once

namespace modules
{

namespace drawing_overlays_module
{

// Return value functions
inline cv::Mat drawRect(cv::Mat img, int x, int y, int w, int h)
{
    cv::Rect rect(x, y, w, h);
    cv::rectangle(img, rect, cv::Scalar(0, 255, 0));
    return img;
}

inline cv::Mat drawText(cv::Mat img, int x, int y, std::string msg)
{
    cv::Point pt_loc(x, y);
    cv::putText(img, msg, pt_loc, cv::FONT_HERSHEY_COMPLEX_SMALL, 0.8, cv::Scalar(0, 200, 250), 1, cv::LINE_AA);
    return img;
}

inline cv::Mat addCross(cv::Mat img, int x, int y, int m_size)
{
    cv::Point pt_loc(x, y);
    cv::drawMarker(img, pt_loc, cv::Scalar(255, 255, 255), cv::MARKER_CROSS, m_size, 1, 8);
    return img;
}

// Pass by reference functions
inline void drawRectOnMat(cv::Mat &img, int x, int y, int w, int h)
{
    cv::Rect rect(x, y, w, h);
    cv::rectangle(img, rect, cv::Scalar(0, 255, 0));
}

inline void drawTextOnMat(cv::Mat &img, int x, int y, std::string msg)
{
    cv::Point pt_loc(x, y);
    cv::putText(img, msg, pt_loc, cv::FONT_HERSHEY_COMPLEX_SMALL, 0.8, cv::Scalar(0, 200, 250), 1, cv::LINE_AA);
}

inline void addCrossOnMat(cv::Mat &img, int x, int y, int m_size)
{
    cv::Point pt_loc(x, y);
    cv::drawMarker(img, pt_loc, cv::Scalar(255, 255, 255), cv::MARKER_CROSS, m_size, 1, 8);
}

} // namespace drawing_overlays_module

} // namespace modules
