#include <iostream>
#include <opencv2/opencv.hpp>

#include "drawing_overlays_module.hpp"

using namespace modules;

// Calculates if two images are the same or not
bool ImagesAreEqual(const cv::Mat &a, const cv::Mat &b)
{
    if ((a.rows != b.rows) || (a.cols != b.cols))
    {
        return false;
    }

    cv::Scalar s = sum(a - b);
    return ((s[0] == 0) && (s[1] == 0) && (s[2] == 0));
}

int main(int argc, char *argv[])
{
    std::cout << "Running unit tests for drawing overlays module..." << std::endl;

    if (argc != 3)
    {
        std::cout << "Could not locate the test images" << std::endl;
        std::cout << "Drawing overlays module unit tests failed" << std::endl;
        return -1;
    }

    // Read images from file system
    cv::Mat testImage;
    cv::Mat ImageFinal;
    testImage = cv::imread(argv[1], cv::IMREAD_COLOR);
    ImageFinal = cv::imread(argv[2], cv::IMREAD_COLOR);
    // testImage = cv::imread("include/modules/drawing_overlays_module/test-images/lena.png", cv::IMREAD_COLOR);
    // ImageFinal = cv::imread("include/modules/drawing_overlays_module/test-images/lena-final.png", cv::IMREAD_COLOR);

    if (!testImage.data || !ImageFinal.data)
    {
        std::cout << "Could not open or find the images" << std::endl;
        std::cout << "Drawing overlays module unit tests failed" << std::endl;
        return -1;
    }

    unsigned int capture_width = testImage.size().width;
    unsigned int capture_height = testImage.size().height;

    // Draw a smaller rectangle overlay
    drawing_overlays_module::drawRectOnMat(testImage, capture_width / 4, capture_height / 4, capture_width / 2, capture_height / 2);
    testImage = drawing_overlays_module::drawRect(testImage, capture_width / 4, capture_height / 4, capture_width / 2, capture_height / 2);

    // Add marker at center
    drawing_overlays_module::addCrossOnMat(testImage, capture_width / 2, capture_height / 2, 20);
    testImage = drawing_overlays_module::addCross(testImage, capture_width / 2, capture_height / 2, 20);

    // Display text
    std::string text = "Hello, World!";
    testImage = drawing_overlays_module::drawText(testImage, 30, 30, text);
    // drawing_overlays_module::drawTextOnMat(testImage, 30, 30, text);

    // Check if images are the same
    if (ImagesAreEqual(testImage, ImageFinal) == false)
    {
        std::cout << "Drawing overlays module unit tests failed" << std::endl;
        return -2;
    }

    std::cout << "Drawing overlays module unit tests passed" << std::endl;
    return 0;
}
