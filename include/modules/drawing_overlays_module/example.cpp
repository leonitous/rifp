#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#include "drawing_overlays_module.hpp"

namespace DrawingOverlays = modules::drawing_overlays_module;

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        std::cout << " Usage: ./example ImageToLoadAndDisplay" << std::endl;
        return -1;
    }

    cv::Mat image;
    image = cv::imread(argv[1], cv::IMREAD_COLOR);

    // Check for invalid input
    if (!image.data)
    {
        std::cout << "Could not open or find the image" << std::endl;
        return -1;
    }

    unsigned int capture_width = image.size().width;
    unsigned int capture_height = image.size().height;

    // Draw a smaller rectangle overlay
    DrawingOverlays::drawRectOnMat(image, capture_width * 0.25, capture_height * 0.25, capture_width * 0.5, capture_height * 0.5);

    // Add marker at center
    DrawingOverlays::addCrossOnMat(image, capture_width / 2, capture_height / 2, 20);

    // Display text
    std::string text = "Hello, World!";
    image = DrawingOverlays::drawText(image, 30, 30, text);

    // Create a window for display.
    cv::namedWindow("Display window", cv::WINDOW_AUTOSIZE);
    // Show our image inside it.
    cv::imshow("Display window", image);

    // Wait for a keystroke in the window
    cv::waitKey(0);
    return 0;
}
