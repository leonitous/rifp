/*
C++ library for performing a similar action as in-range on the gpu
*/

#pragma once

#include <opencv2/core/cuda.hpp>
#include <opencv2/core/mat.hpp>
#include <opencv2/cudaarithm.hpp>
#include <opencv2/cudaimgproc.hpp>
#include <opencv2/imgproc.hpp>

namespace modules
{

namespace gpu_threshold_module
{

// all the parts to keep low and high thresholds
static cv::cuda::GpuMat tmp1;
static cv::cuda::GpuMat done;
static cv::cuda::GpuMat mat_parts[3];
static cv::cuda::GpuMat mat_parts_low[3];
static cv::cuda::GpuMat mat_parts_high[3];

inline void gpuThreshold(const cv::Scalar hsv_low, const cv::Scalar hsv_high, cv::cuda::GpuMat &input)
{
    // split into 3 channels H, S and V
    cv::cuda::split(input, mat_parts);

    // Use the inverse binary of upper threshold with bitwise_and to remove above limit values from lower threshold.
    // find range between high and low values of H
    cv::cuda::threshold(mat_parts[0], mat_parts_low[0], hsv_low[0], 255, cv::THRESH_BINARY);
    cv::cuda::threshold(mat_parts[0], mat_parts_high[0], hsv_high[0], 255, cv::THRESH_BINARY_INV);
    cv::cuda::bitwise_and(mat_parts_high[0], mat_parts_low[0], mat_parts[0]);

    // find range between high and low values of S
    cv::cuda::threshold(mat_parts[1], mat_parts_low[1], hsv_low[1], 255, cv::THRESH_BINARY);
    cv::cuda::threshold(mat_parts[1], mat_parts_high[1], hsv_high[1], 255, cv::THRESH_BINARY_INV);
    cv::cuda::bitwise_and(mat_parts_high[1], mat_parts_low[1], mat_parts[1]);

    // find range between high and low values of V
    cv::cuda::threshold(mat_parts[2], mat_parts_low[2], hsv_low[2], 255, cv::THRESH_BINARY);
    cv::cuda::threshold(mat_parts[2], mat_parts_high[2], hsv_high[2], 255, cv::THRESH_BINARY_INV);
    cv::cuda::bitwise_and(mat_parts_high[2], mat_parts_low[2], mat_parts[2]);

    // bitwise AND all channels.
    cv::cuda::bitwise_and(mat_parts[0], mat_parts[1], tmp1);
    cv::cuda::bitwise_and(tmp1, mat_parts[2], input);
}

inline cv::cuda::GpuMat gpuThreshold(const cv::Scalar hsv_low, const cv::Scalar hsv_high, const cv::cuda::GpuMat input)
{
    // split into 3 channels H, S and V
    cv::cuda::split(input, mat_parts);

    // Use the inverse binary of upper threshold with bitwise_and to remove above limit values from lower threshold.
    // find range between high and low values of H
    cv::cuda::threshold(mat_parts[0], mat_parts_low[0], hsv_low[0], 255, cv::THRESH_BINARY);
    cv::cuda::threshold(mat_parts[0], mat_parts_high[0], hsv_high[0], 255, cv::THRESH_BINARY_INV);
    cv::cuda::bitwise_and(mat_parts_high[0], mat_parts_low[0], mat_parts[0]);

    // find range between high and low values of S
    cv::cuda::threshold(mat_parts[1], mat_parts_low[1], hsv_low[1], 255, cv::THRESH_BINARY);
    cv::cuda::threshold(mat_parts[1], mat_parts_high[1], hsv_high[1], 255, cv::THRESH_BINARY_INV);
    cv::cuda::bitwise_and(mat_parts_high[1], mat_parts_low[1], mat_parts[1]);

    // find range between high and low values of V
    cv::cuda::threshold(mat_parts[2], mat_parts_low[2], hsv_low[2], 255, cv::THRESH_BINARY);
    cv::cuda::threshold(mat_parts[2], mat_parts_high[2], hsv_high[2], 255, cv::THRESH_BINARY_INV);
    cv::cuda::bitwise_and(mat_parts_high[2], mat_parts_low[2], mat_parts[2]);

    // bitwise AND all channels.
    cv::cuda::bitwise_and(mat_parts[0], mat_parts[1], tmp1);
    cv::cuda::bitwise_and(tmp1, mat_parts[2], done);

    return done;
}

inline void gpuThreshold(const cv::Scalar hsv_low, const cv::Scalar hsv_high, const cv::cuda::GpuMat input, cv::cuda::GpuMat &output)
{
    // split into 3 channels H, S and V
    cv::cuda::split(input, mat_parts);

    // Use the inverse binary of upper threshold with bitwise_and to remove above limit values from lower threshold.
    // find range between high and low values of H
    cv::cuda::threshold(mat_parts[0], mat_parts_low[0], hsv_low[0], 255, cv::THRESH_BINARY);
    cv::cuda::threshold(mat_parts[0], mat_parts_high[0], hsv_high[0], 255, cv::THRESH_BINARY_INV);
    cv::cuda::bitwise_and(mat_parts_high[0], mat_parts_low[0], mat_parts[0]);

    // find range between high and low values of S
    cv::cuda::threshold(mat_parts[1], mat_parts_low[1], hsv_low[1], 255, cv::THRESH_BINARY);
    cv::cuda::threshold(mat_parts[1], mat_parts_high[1], hsv_high[1], 255, cv::THRESH_BINARY_INV);
    cv::cuda::bitwise_and(mat_parts_high[1], mat_parts_low[1], mat_parts[1]);

    // find range between high and low values of V
    cv::cuda::threshold(mat_parts[2], mat_parts_low[2], hsv_low[2], 255, cv::THRESH_BINARY);
    cv::cuda::threshold(mat_parts[2], mat_parts_high[2], hsv_high[2], 255, cv::THRESH_BINARY_INV);
    cv::cuda::bitwise_and(mat_parts_high[2], mat_parts_low[2], mat_parts[2]);

    // bitwise AND all channels.
    cv::cuda::bitwise_and(mat_parts[0], mat_parts[1], tmp1);
    cv::cuda::bitwise_and(tmp1, mat_parts[2], output);
}

} // namespave gpu_threshold_module

} // namespace modules
