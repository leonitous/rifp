/*
C++ library for creating opencv compatible gstreamer pipelines
https://gitlab.com/leonitous/cpp-gstreamer-pipelines
*/

#pragma once

#include <string>
#include <vector>

namespace modules
{

class OpencvGStreamerPipeline
{
  public:
    OpencvGStreamerPipeline(std::string inputSource);
    virtual ~OpencvGStreamerPipeline();
    std::string getPipelineString();

    void setStreamerSettings(std::vector<std::string> settings);
    void addSink(std::string sink);
    void addSource(std::string source);
    void addElement(std::string sink, std::string source);

  private:
    std::string pipeline = "";
};

OpencvGStreamerPipeline::OpencvGStreamerPipeline(std::string inputSource)
{
    pipeline.append(inputSource);
}

OpencvGStreamerPipeline::~OpencvGStreamerPipeline() {}

std::string OpencvGStreamerPipeline::getPipelineString()
{
    return (pipeline + " ! appsink");
}

void OpencvGStreamerPipeline::setStreamerSettings(std::vector<std::string> settings)
{
    std::string params = "";
    for (const auto &piece : settings)
        params += (piece + ", ");

    params.pop_back();
    params.pop_back();
    addSource(params);
}

void OpencvGStreamerPipeline::addSink(std::string sink)
{
    // pipeline.append((const char[4]) " ! ");
    pipeline.append(" ! ");
    pipeline.append(sink);
}

void OpencvGStreamerPipeline::addSource(std::string source)
{
    // pipeline.append((const char[4]) " ! ");
    pipeline.append(" ! ");
    pipeline.append(source);
}

void OpencvGStreamerPipeline::addElement(std::string sink, std::string source)
{
    addSink(sink);
    addSource(source);
}

} // namespace modules
