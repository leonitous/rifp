#include <iostream>
#include <string>
#include <vector>

#include "gstreamer_pipelines.hpp"

using OpencvGStreamerPipeline = modules::OpencvGStreamerPipeline;

// Global recording variables
constexpr int capture_width{1280};
constexpr int capture_height{720};
constexpr int framerate{120};
constexpr int flip_method{0};

// GStreamer pipeline settings
const std::vector<std::string> streamerSourceParams = {"video/x-raw(memory:NVMM)", "width=(int)" + std::to_string(capture_width),
                                                       "height=(int)" + std::to_string(capture_height), "format=(string)NV12",
                                                       "framerate=(fraction)" + std::to_string(framerate) + "/1"};

// Expected output
const std::string expectedOutput = "nvarguscamerasrc ! video/x-raw(memory:NVMM), width=(int)1280, "
                                   "height=(int)720, format=(string)NV12, framerate=(fraction)120/1 ! "
                                   "nvvidconv ! video/x-raw, format=(string)BGRx ! videoconvert ! "
                                   "video/x-raw, format=(string)BGR ! appsink";

int main(int argc, char *argv[])
{
    std::cout << "Running unit tests for GStreamer Pipelines module..." << std::endl;

    // Make the pipeline object with the streamer
    OpencvGStreamerPipeline opencvGSpipeline("nvarguscamerasrc");
    opencvGSpipeline.setStreamerSettings(streamerSourceParams);

    // Add a sink and a source to process the sink
    opencvGSpipeline.addSink("nvvidconv");
    opencvGSpipeline.addSource("video/x-raw, format=(string)BGRx");

    // Add a sink and source together
    std::string secondSink = "videoconvert";
    std::string secondSource = "video/x-raw, format=(string)BGR";
    opencvGSpipeline.addElement(secondSink, secondSource);

    // Get the pipeline string to pass to opencv
    std::string GSpipeline = opencvGSpipeline.getPipelineString();

    if (expectedOutput.compare(GSpipeline) != 0)
    {
        std::cout << "GStreamer Pipelines module unit tests failed" << std::endl;
        std::cout << "Expected output was: " << expectedOutput << std::endl;
        std::cout << "Got: " << GSpipeline << std::endl;
        return -1;
    }

    std::cout << "GStreamer Pipelines module unit tests passed" << std::endl;
    return 0;
}
