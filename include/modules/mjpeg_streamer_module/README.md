# mjpeg streamer

C++ MJPEG over HTTP Library

## Example

```c++
#include <opencv2/opencv.hpp>

#include "mjpeg_streamer_module.hpp"

using MJPEGStreamer = modules::MJPEG_streamer_module;

int main(int argc, char *argv[])
{
    cv::VideoCapture cap(0);
    if (!cap.isOpened())
    {
        std::cerr << "VideoCapture not opened\n";
        exit(EXIT_FAILURE);
    }

    std::vector<int> params = {cv::IMWRITE_JPEG_QUALITY, 90};

    MJPEGStreamer streamer;

    // By default "/shutdown" is the target to graceful shutdown the streamer
    // if you want to change the target to graceful shutdown:
    //      streamer.setShutdownTarget("/stop");

    // By default 1 worker is used for streaming
    // if you want to use 4 workers:
    //      streamer.start(8080, 4);
    streamer.start(8080);
    cv::Mat frame;

    // Visit /shutdown or another defined target to stop the loop and graceful shutdown
    while (streamer.isAlive())
    {
        cap >> frame;
        if (frame.empty())
        {
            std::cerr << "frame not grabbed\n";
            exit(EXIT_FAILURE);
        }

        // http://localhost:8080/bgr
        std::vector<uchar> buff_bgr;
        cv::imencode(".jpg", frame, buff_bgr, params);
        streamer.publish("/bgr", std::string(buff_bgr.begin(), buff_bgr.end()));

        cv::Mat hsv;
        cv::cvtColor(frame, hsv, cv::COLOR_BGR2HSV);

        // http://localhost:8080/hsv
        std::vector<uchar> buff_hsv;
        cv::imencode(".jpg", hsv, buff_hsv, params);
        streamer.publish("/hsv", std::string(buff_hsv.begin(), buff_hsv.end()));
    }

    streamer.stop();
}
```

### Compile Example

```sh
g++ -g -Wall example.cpp -o example -std=c++17 -lpthread $(pkg-config --cflags --libs opencv4)
./example
```

## Run tests

```sh
g++ -g -Wall tests.cpp -o test -std=c++17 -lpthread -lcurl $(pkg-config --cflags --libs opencv4)
./test ./test-images/lena.png
```
