/*
C++ library for avoiding the upload and download performance penalty in opencv
of sending a Mat (image) to and from a gpu.
*/

#pragma once

#include <iostream>
#include <vector>

#include "utils.hpp"
#include <cuda_runtime.h>

// Comment out this definition for using pinned memory instead of unified memory
#define USE_UNIFIED_MEM

namespace modules
{

namespace fast_gpumat_module
{

// See: https://forums.developer.nvidia.com/t/eliminate-upload-download-for-opencv-cuda-gpumat-using-shared-memory/83090/3
std::vector<void *> allocateMat()
{
    unsigned int pixels = utils::capture_width * utils::capture_height;
    unsigned int frameByteSize = pixels * 3;
    std::vector<void *> ptrs;

#ifndef USE_UNIFIED_MEM
    /* Pinned memory. No cache */
    void *device_ptr, *host_ptr;
    cudaSetDeviceFlags(cudaDeviceMapHost);
    cudaHostAlloc((void **)&host_ptr, frameByteSize, cudaHostAllocMapped);
    cudaHostGetDevicePointer((void **)&device_ptr, (void *)host_ptr, 0);
    ptrs.push_back(host_ptr);
    ptrs.push_back(device_ptr);
#else
    /* Unified memory */
    void *unified_ptr;
    cudaMallocManaged(&unified_ptr, frameByteSize);
    ptrs.push_back(unified_ptr);
    ptrs.push_back(unified_ptr);
#endif

    return ptrs;
}

} // namespace fast_gpumat_module

} // namespace modules
