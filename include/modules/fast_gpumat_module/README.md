# Fast Gpu-Mat

C++ library for avoiding the upload and download performance penalty in opencv of sending a Mat (image) to and from a gpu for processing. Only available on Nvidia Jetson devices.

## C++ Example

```cpp
#include <iostream>
#include <opencv2/opencv.hpp>

#include "fast_gpumat_module.hpp"

namespace fastGpuMat = modules::fast_gpumat_module;

int main(int argc, char *argv[])
{
    // Check input
    if (argc != 2)
    {
        std::cout << " Usage: ./example ImageToLoadAndDisplay" << std::endl;
        return -1;
    }

    // Create a test mat on the host device
    cv::Mat originalImage;
    originalImage = cv::imread(argv[1], cv::IMREAD_COLOR);

    // Check for invalid input
    if (!originalImage.data)
    {
        std::cout << "Could not open or find the image" << std::endl;
        return -1;
    }

    // Setup size
    unsigned int capture_width = originalImage.size().width;
    unsigned int capture_height = originalImage.size().height;

    // Allocate unified memory for mat
    std::vector<void *> MatPtrs = fastGpuMat::allocateMat();

    /**
     * Setup Mats - one on host one on device. Both these Mats share the same data and the
     * data does not need to be copied between them
     */
    cv::Mat frame(capture_height, capture_width, CV_8UC3, MatPtrs.at(0));
    cv::cuda::GpuMat d_frame(capture_height, capture_width, CV_8UC3, MatPtrs.at(1));

    // Load data into mats
    frame = cv::imread(argv[1], cv::IMREAD_COLOR);

    // Do some processing with the data in the Mat on the GPU


    return 0;
}
```

### Compile Example File

```sh
g++ -g -Wall example.cpp -o example -std=c++11 `pkg-config --cflags --libs opencv4` -I/usr/local/cuda/targets/aarch64-linux/include -L/usr/local/cuda/targets/aarch64-linux/lib -lcudart
./example
```

### Compile Tests

```sh
g++ -g -Wall tests.cpp -o test -std=c++11 `pkg-config --cflags --libs opencv4` -I/usr/local/cuda/targets/aarch64-linux/include -L/usr/local/cuda/targets/aarch64-linux/lib -lcudart
./test ./test-images/lena.png
```
