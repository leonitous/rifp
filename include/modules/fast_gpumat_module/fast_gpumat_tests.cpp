#include <iostream>
#include <opencv2/core/cuda.hpp>
#include <opencv2/core/cvstd.hpp>
#include <opencv2/cudaarithm.hpp>
#include <opencv2/cudafilters.hpp>
#include <opencv2/opencv.hpp>

#include "fast_gpumat_module.hpp"

namespace fastGpuMat = modules::fast_gpumat_module;

#define DEBUG false

// Calculates if two images are the same or not
bool ImagesAreEqual(const cv::Mat &a, const cv::Mat &b)
{
    if ((a.rows != b.rows) || (a.cols != b.cols))
    {
        return false;
    }

    cv::Scalar s = sum(a - b);
    return ((s[0] == 0) && (s[1] == 0) && (s[2] == 0));
}

int main(int argc, char *argv[])
{
    std::cout << "Running unit tests for fast gpumat module..." << std::endl;

    if (argc != 3)
    {
        std::cout << "Could not locate the test images" << std::endl;
        std::cout << "Fast gpumat module unit tests failed" << std::endl;
        return -1;
    }

    // Create a test mat on the host device
    cv::Mat originalImage;
    cv::Mat imageFinal;
    originalImage = cv::imread(argv[1], cv::IMREAD_COLOR);
    imageFinal = cv::imread(argv[2], cv::IMREAD_COLOR);

    // Check for invalid input
    if ((!originalImage.data) || (!imageFinal.data))
    {
        std::cout << "Could not open or find the test images" << std::endl;
        return -1;
    }

    // Check cuda compatability
    int cudaCompatible = cv::cuda::getCudaEnabledDeviceCount();

    // Setup size
    unsigned int capture_width = originalImage.size().width;
    unsigned int capture_height = originalImage.size().height;

    // Allocate unified memory for mat
    std::vector<void *> MatPtrs = fastGpuMat::allocateMat();

    /**
     * Setup Mats - one on host one on device. Both these Mats share the same data and the
     * data does not need to be copied between them
     */
    cv::Mat frame_out(capture_height, capture_width, CV_8UC3, MatPtrs.at(0));
    cv::cuda::GpuMat d_frame_out(capture_height, capture_width, CV_8UC3, MatPtrs.at(1));

    // Load data into mats
    originalImage.copyTo(frame_out);

    if (DEBUG)
    {
        std::cout << "Fast gpumat cuda compatability=" << cudaCompatible << std::endl;
        std::cout << "host pointer: " << MatPtrs.at(0) << std::endl;
        std::cout << "device pointer: " << MatPtrs.at(1) << std::endl;
    }

    if (cudaCompatible == -1)
    {
        std::cout << "Fast gpumat module unit tests could not be ran" << std::endl;
        std::cout << "Cuda driver and Cuda runtime version mismatch" << std::endl;
    }
    else if (cudaCompatible == 0)
    {
        std::cout << "Fast gpumat module unit tests could not be ran" << std::endl;
        std::cout << "There is no cuda enabled gpu available to the system to utilize for the tests" << std::endl;
    }
    else if (cudaCompatible > 0)
    {
        // Perform a GPU operation of some sort. Using sobel filter for simple placeholder
        cv::Ptr<cv::cuda::Filter> filter = cv::cuda::createSobelFilter(CV_8UC3, CV_8UC3, 1, 1, 1, 1, cv::BORDER_DEFAULT);
        filter->apply(d_frame_out, d_frame_out);

        if (DEBUG)
        {
            cv::imwrite("/rifp/test.png", frame_out);
        }
    }

    // Compare images
    if (ImagesAreEqual(frame_out, imageFinal) == false)
    {
        std::cout << "Fast gpumat module unit tests failed" << std::endl;
        std::cout << "Host and original images were not the same after applying filter to device frame" << std::endl;

        // Free memory
        cudaFree(MatPtrs.at(0));
        cudaFree(MatPtrs.at(1));
        return -2;
    }

    // Free memory
    cudaFree(MatPtrs.at(0));
    cudaFree(MatPtrs.at(1));

    if (cudaCompatible > 0)
    {
        std::cout << "Fast gpumat module unit tests passed" << std::endl;
        return 0;
    }
}
