#pragma once

#include <opencv2/core/types.hpp>
#include <string>
#include <vector>

// Point types
typedef cv::Point_<int> Point2i;
typedef Point2i Point;
typedef cv::Point_<float> Point2f;
typedef cv::Point_<double> Point2d;

namespace utils
{

// Global recording variables
constexpr unsigned int capture_width{1280};
constexpr unsigned int capture_height{720};
constexpr unsigned int framerate{120};
constexpr unsigned int flip_method{0};

// GStreamer pipeline settings
const std::vector<std::string> streamerSourceParams = {"video/x-raw(memory:NVMM)", "width=(int)" + std::to_string(capture_width),
                                                       "height=(int)" + std::to_string(capture_height), "format=(string)NV12",
                                                       "framerate=(fraction)" + std::to_string(framerate) + "/1"};
const std::string nvvidconvSink = "nvvidconv flip-method=" + std::to_string(flip_method);
const std::string nvvidconvSource = "video/x-raw, format=(string)BGRx";
const std::string videoconvertSink = "videoconvert";
const std::string videoconvertSource = "video/x-raw, format=(string)BGR";

// Streamer information
constexpr unsigned int streamer_port{8080};
const std::string streamer_shutdown = "/shutdown";

} // namespace utils
