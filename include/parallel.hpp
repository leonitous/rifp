#pragma once

#include <iostream>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

namespace parallelComputing
{

class Parallel_process : public cv::ParallelLoopBody
{

  private:
    cv::Mat &inputFrame;
    cv::Mat &outputFrame;

  public:
    Parallel_process(cv::Mat &inFrame, cv::Mat &outFrame, int st) : inputFrame(inFrame), outputFrame(outFrame) {}

    virtual void operator()(const cv::Range &range) const
    {
        for (int i = range.start; i < range.end; i++)
        {

        }
    }
};

} // namespace parallelComputing
