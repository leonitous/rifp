/*
C++ library for interfacing with an odrive over usb using
a python host program
*/

#include <boost/python.hpp>

#include <iostream>
#include <vector>

namespace movement
{

class odrive
{
  private:
    boost::python::object odriveLibrary;
    boost::python::object connectedOdrive;
    boost::python::dict connectedOdriveProperties;

    boost::python::object extractOdriveAttribute(const std::string axis, const std::vector<std::string> keys)
    {
        // Temp references
        boost::python::object temp = this->connectedOdriveProperties[axis];

        // Loop and chain all the attributes
        std::for_each(keys.begin(), keys.end(), [&](std::string const &elem) {
            // if the current index is needed:
            auto i = &elem - &keys[0];

            // cannot continue, break or return out of the loop
            temp = temp.attr(elem.c_str());
        });

        return temp;
    }

  public:
    odrive();
    virtual ~odrive();

    // set a param to a value
    template <typename Type> void execute(const std::string axis, const std::vector<std::string> keys, Type value) const;
    template <typename Type> Type query(const std::string axis, const std::vector<std::string> keys);
    template <typename Type> void query(const std::string axis, const std::vector<std::string> keys, Type &result);
};

} // namespace modules
