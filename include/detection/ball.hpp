/*
detects the position of the ball from a calibrated frame
*/

#include "modules/cam_fps_module/fps_counter.hpp"
#include "modules/gpu_threshold_module/gpu_threshold_module.hpp"
#include "utils.hpp"

#include <opencv2/core/cuda.hpp>
#include <opencv2/core/hal/interface.h>
#include <opencv2/core/mat.hpp>
#include <opencv2/cudafilters.hpp>
#include <opencv2/cudaimgproc.hpp>

#pragma once

namespace detection
{

namespace ball
{

static cv::Scalar lowerBall_hsv(0, 0, 0);
static cv::Scalar upperBall_hsv(180, 255, 255);
static modules::Interval processingTimeInterval;

const cv::Mat smallKernel = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(3, 3));
const cv::Mat medKernel = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(11, 11));
const cv::Mat largeKernel = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(21, 21));
const cv::Ptr<cv::cuda::Filter> openingFilter = cv::cuda::createMorphologyFilter(cv::MORPH_OPEN, CV_8UC1, medKernel);
const cv::Ptr<cv::cuda::Filter> erodeFilter = cv::cuda::createMorphologyFilter(cv::MORPH_ERODE, CV_8UC1, smallKernel);
const cv::Ptr<cv::cuda::Filter> dilateFilter = cv::cuda::createMorphologyFilter(cv::MORPH_DILATE, CV_8UC1, largeKernel);

void calibrateBallDetection(const cv::Mat frame);
const void findBall(Point2f &ball_pos, unsigned int &ballProcessingTime, const cv::Mat input, const cv::cuda::GpuMat d_input,
                    cv::Mat &output, cv::cuda::GpuMat &d_output);

} // namespace ball

} // namespace detection
