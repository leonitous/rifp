# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## (in development) [Unreleased] - current

## (pre release) [0.0.1] - 2020-07-30

### Added

- Camera FPS module to measure the processing speed of the system
- Drawing overlays module to provide simple image overlays to the processed images
- Fast GPU Mat module for loading an image onto the embedded GPU faster
- Gstreamer pipelines module for creating easier gstreamer pipelines and connecting to the camera
- MJPEG streamer module for streaming the processed images to a web browser
- Module tests and Module examples
- Opencv Cuda linker test
- Started Documentation with mkdocs and readthedocs.org
- Arduino code for controlling the LED rings and scripts for uploading to arduino remotely
- Scripts to upload full codebase to Jetson Nano
- Script for testing, building, and cleaning
- Gitlab CI integration
- Docker Images for faster testing
- 120fps processing

The system runs at 120fps now. This is a pre release because there are no detection capabilities right now as they broke when I updated to running @ 120fps. The next task will be porting over the old detection methods that broke - they will be included in the next update/release

[Unreleased]: https://gitlab.com/leonitous/rifp/compare/v0.0.1...master
[0.0.1]: https://gitlab.com/leonitous/rifp/-/releases/v0.0.1
