#include <iostream>

#include "algorithms/track-and-smash.hpp"

namespace algorithms
{

namespace trackAndSpace
{

void perform()
{
    std::cout << "Doing track and smash..." << std::endl;
}

} // namespace trackAndSpace

} // namespace algorithms
