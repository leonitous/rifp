/*
C++ library for interfacing with an odrive over usb using
a python host program
*/

#include "movement/odrive.hpp"

namespace movement
{

odrive::odrive()
{
    // initialize Python.
    Py_Initialize();

    // import the odrive library
    odriveLibrary = boost::python::import("odrive");

    // connect to the odrive
    std::cout << "Waiting for odrive to connect..." << std::endl;
    connectedOdrive = odriveLibrary.attr("find_any")();
    std::cout << "Odrive successfully connected!" << std::endl;
    connectedOdriveProperties = boost::python::extract<boost::python::dict>(connectedOdrive.attr("__dict__"));
}
odrive::~odrive()
{
    // Do not call Py_Finalize with Boost.Python! my bad
    // Py_Finalize();
}

// set a param to a value
template <typename Type> void odrive::execute(const std::string axis, const std::vector<std::string> keys, Type value) const
{
    std::vector<const char *> charVec(keys.size(), nullptr);
    for (int i = 0; i < keys.size(); i++)
    {
        charVec[i] = keys[i].c_str();
    }

    if (keys.size() == 1)
    {
        this->connectedOdriveProperties[axis].attr(charVec[0]) = value;
    }
    else if (keys.size() == 2)
    {
        this->connectedOdriveProperties[axis].attr(charVec[0]).attr(charVec[1]) = value;
    }
    else if (keys.size() == 3)
    {
        this->connectedOdriveProperties[axis].attr(charVec[0]).attr(charVec[1]).attr(charVec[2]) = value;
    }
    else if (keys.size() == 4)
    {
        this->connectedOdriveProperties[axis].attr(charVec[0]).attr(charVec[1]).attr(charVec[2]).attr(charVec[3]) = value;
    }
    else
    {
        std::cout << "Too many arguments supplied!" << std::endl;
    }
}

template <typename Type> Type odrive::query(const std::string axis, const std::vector<std::string> keys)
{
    // Extract the attribute
    boost::python::object attribute = this->extractOdriveAttribute(axis, keys);
    // Cast the value
    return boost::python::extract<Type>(attribute);
}

template <typename Type> void odrive::query(const std::string axis, const std::vector<std::string> keys, Type &result)
{
    // Extract the attribute
    boost::python::object attribute = this->extractOdriveAttribute(axis, keys);
    // Cast the value
    result = boost::python::extract<Type>(attribute);
}

} // namespace movement
