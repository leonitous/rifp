// Standard library headers
#include <chrono>
#include <ctime>

// Opencv header file
#include <opencv2/core/cuda.hpp>
#include <opencv2/core/mat.hpp>
#include <opencv2/cudaarithm.hpp>
#include <opencv2/cudafilters.hpp>
#include <opencv2/cudaimgproc.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/opencv.hpp>

// Our module header files
#include "modules/arduino_serial_communication_module/arduino_serial_communication.h"
#include "modules/cam_fps_module/fps_counter.hpp"
#include "modules/drawing_overlays_module/drawing_overlays_module.hpp"
#include "modules/fast_gpumat_module/fast_gpumat_module.hpp"
#include "modules/gpu_threshold_module/gpu_threshold_module.hpp"
#include "modules/gstreamer_pipelines_module/gstreamer_pipelines.hpp"
#include "modules/mjpeg_streamer_module/mjpeg_streamer_module.hpp"
#include "modules/odrive_communication_module/odrive_communication.hpp"

// Our other header files
#include "algorithms/pid-controller.hpp"
#include "algorithms/track-and-smash.hpp"
#include "movement/odrive.hpp"
#include "parallel.hpp"
#include "utils.hpp"

// Detection methods
#include "detection/ball.hpp"
#include "detection/players.hpp"
#include "detection/table.hpp"

// Using declaractions
using odrive = modules::odrive;
using Interval = modules::Interval;
using FpsCounter = modules::Fps_counter;
using MJPEGStreamer = modules::MJPEG_streamer_module;
using GstreamerPipeline = modules::OpencvGStreamerPipeline;
using ParallelProcess = parallelComputing::Parallel_process;
namespace drawingOverlays = modules::drawing_overlays_module;
namespace fastGpuMat = modules::fast_gpumat_module;

int main(int argc, char **argv)
{
    // Init objects
    FpsCounter fps;
    GstreamerPipeline GSpipeline("nvarguscamerasrc");
    MJPEGStreamer MjpegStreamer;

    // Setup pipeline
    GSpipeline.setStreamerSettings(utils::streamerSourceParams);

    // Add nvvidconv source and sink and videoconvert source and sink
    GSpipeline.addElement(utils::nvvidconvSink, utils::nvvidconvSource);
    GSpipeline.addElement(utils::videoconvertSink, utils::videoconvertSource);
    GSpipeline.addSink("queue");

    // Init opencv capture
    cv::VideoCapture cap(GSpipeline.getPipelineString(), cv::CAP_GSTREAMER);
    if (!cap.isOpened())
    {
        std::cerr << "Failed to open camera" << std::endl;
        exit(EXIT_FAILURE);
    }

    // Setup our odrive connection
    odrive odrv0;

    // Start streamer
    MjpegStreamer.start(utils::streamer_port);
    MjpegStreamer.setShutdownTarget(utils::streamer_shutdown);

    // Ask for full calibration sequence
    odrv0.execute<int>("axis0", std::vector<std::string>{"requested_state"}, 3);
    while (odrv0.query<int>("axis0", std::vector<std::string>{"current_state"}) != 1)
    {
        usleep(1000);
    }
    sleep(1);

    // Ask for encoder homing process
    odrv0.execute<int>("axis0", std::vector<std::string>{"requested_state"}, 11);
    while (odrv0.query<int>("axis0", std::vector<std::string>{"current_state"}) != 1)
    {
        usleep(1000);
    }
    sleep(1);

    // Put odrive into closed loop control mode
    odrv0.execute<int>("axis0", std::vector<std::string>{"requested_state"}, 8);

    // Allocate unified memory for frame mat
    std::vector<void *> frameMatPtrs = fastGpuMat::allocateMat();
    cv::Mat frame(utils::capture_height, utils::capture_width, CV_8UC3, frameMatPtrs.at(0));
    cv::cuda::GpuMat d_frame(utils::capture_height, utils::capture_width, CV_8UC3, frameMatPtrs.at(1));

    // Allocate unified memory for hsv mat
    std::vector<void *> hsvMatPtrs = fastGpuMat::allocateMat();
    cv::Mat hsvFrame(utils::capture_height, utils::capture_width, CV_8UC3, hsvMatPtrs.at(0));
    cv::cuda::GpuMat d_hsvFrame(utils::capture_height, utils::capture_width, CV_8UC3, hsvMatPtrs.at(1));

    // Allocate unified memory for ball mask mat
    std::vector<void *> ballMaskMatPtrs = fastGpuMat::allocateMat();
    cv::Mat ballMask(utils::capture_height, utils::capture_width, CV_8UC1, ballMaskMatPtrs.at(0));
    cv::cuda::GpuMat d_ballMask(utils::capture_height, utils::capture_width, CV_8UC1, ballMaskMatPtrs.at(1));

    // Inverted frame mat
    // cv::cuda::GpuMat d_invertedFrame;
    cv::Mat invertedFrame;

    // Local variables
    Interval encodingInterval;
    std::vector<uchar> buff_bgr;
    std::vector<uchar> buff_hsv;
    std::vector<uchar> buff_ball;
    bool colorIsCalibrated = false;
    unsigned int ballProcessingTime = 0;
    float ball_pos_in_motor_rotations = 0;
    unsigned int lastFrameEncodingTime = 0;
    std::vector<int> params = {cv::IMWRITE_JPEG_QUALITY, 60};
    Point2f ball(utils::capture_width / 2, utils::capture_height / 2);

    // Flush output stream before loop
    std::cout << "\n";

    // Image processing loop
    while (true)
    {
        // Read a frame
        cap >> frame;
        if (frame.empty())
        {
            std::cerr << "Frame not grabbed" << std::endl;
            exit(EXIT_FAILURE);
        }

        // Standard image processing
        // cv::cuda::bitwise_not(d_frame, d_invertedFrame);
        // cv::cuda::cvtColor(d_invertedFrame, d_hsvFrame, cv::COLOR_BGR2HSV);
        cv::bitwise_not(frame, invertedFrame);
        cv::cvtColor(invertedFrame, hsvFrame, cv::COLOR_BGR2HSV);

        // Find the ball
        detection::ball::findBall(ball, ballProcessingTime, hsvFrame, d_hsvFrame, ballMask, d_ballMask);

        // send data to odrive
        ball_pos_in_motor_rotations = ((float)ball.y / (float)720) * ((float)56000 / (float)2400);
        odrv0.execute("axis0", std::vector<std::string> {"controller", "input_pos"}, ball_pos_in_motor_rotations);

        // Calculate fps
        fps.update();

        // Do this approx 5-6 times a second
        // (not necessary to do this for all frames)
        if (fps.getFrameCount() % 20 == 0)
        {
            // Time the encoding process
            encodingInterval.reset();

            // Encode image for streamer only if there is clients connected
            if (MjpegStreamer.numClientsConnectedTo("/stream") >= 1)
            {
                // Add marker at ball marker
                drawingOverlays::addCrossOnMat(frame, ball.x, ball.y, 20);
                std::string pos_ball = "Ball POS: (" + std::to_string(ball.x) + "px, " + std::to_string(ball.y) +
                                       "px), BPS: " + std::to_string(ballProcessingTime) + "us";
                drawingOverlays::drawTextOnMat(frame, 30, 76, pos_ball);

                // Display fps
                std::string t_fps = "FPS: " + std::to_string(fps.get()) + ", " + std::to_string(1000 / fps.get()) + "ms";
                drawingOverlays::drawTextOnMat(frame, 30, 30, t_fps);

                // Display frame encoding time
                std::string t_fet = "FET: " + std::to_string(lastFrameEncodingTime) + "us, QUALITY: " + std::to_string(params[1]) + "%";
                drawingOverlays::drawTextOnMat(frame, 30, 53, t_fet);

                // Encode the frame
                cv::imencode(".jpg", frame, buff_bgr, params);
                MjpegStreamer.publish("/stream", std::string(buff_bgr.begin(), buff_bgr.end()));
            }
            else
            {
                if (fps.getFrameCount() == 0)
                {
                    std::cout << "FPS: " << std::to_string(fps.get()) << ", " + std::to_string(1000 / fps.get()) << "ms" << std::endl;
                    std::cout << "BPT: " << std::to_string(ballProcessingTime) << "us" << std::endl;
                }
            }
            if (MjpegStreamer.numClientsConnectedTo("/ball") >= 1)
            {
                cv::imencode(".jpg", ballMask, buff_ball, params);
                MjpegStreamer.publish("/ball", std::string(buff_ball.begin(), buff_ball.end()));
            }
            // Perform ball detection calibration when requested
            if ((MjpegStreamer.numClientsConnectedTo("/calibrate_ball_detection") >= 1))
            {
                if (colorIsCalibrated)
                {
                    cv::imencode(".jpg", hsvFrame, buff_hsv, params);
                    MjpegStreamer.publish("/calibrate_ball_detection", std::string(buff_hsv.begin(), buff_hsv.end()));
                }
                else if (!colorIsCalibrated)
                {
                    detection::ball::calibrateBallDetection(hsvFrame);
                    colorIsCalibrated = true;
                }
            }

            lastFrameEncodingTime = encodingInterval.value();
        }

        if (!MjpegStreamer.isAlive())
            break;
    }

    // Cleanup
    odrv0.execute<int>("axis0", std::vector<std::string> {"requested_state"}, 1);
    MjpegStreamer.stop();
    cap.release();
    return 0;
}
