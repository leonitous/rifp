#include <algorithm>
#include <cmath>
#include <iostream>
#include <opencv2/imgproc.hpp>

#include "detection/ball.hpp"

namespace detection
{

namespace ball
{

const void findBall(Point2f &ball_pos, unsigned int &ballProcessingTime, const cv::Mat input, const cv::cuda::GpuMat d_input,
                    cv::Mat &output, cv::cuda::GpuMat &d_output)
{
    float ballRadius = 0;
    std::vector<cv::Vec4i> hierarchy;
    std::vector<Point> largestContour;
    std::vector<std::vector<Point>> contours;

    processingTimeInterval.reset();

    // modules::gpu_threshold_module::gpuThreshold(lowerBall_hsv, upperBall_hsv, d_input, d_output);
    cv::inRange(input, lowerBall_hsv, upperBall_hsv, output);

    cv::erode(input, input, smallKernel);
    cv::dilate(input, input, largeKernel);
    // erodeFilter->apply(d_output, d_output);
    // dilateFilter->apply(d_output, d_output);

    int largestArea = 0;
    cv::findContours(output, contours, hierarchy, cv::RETR_TREE, cv::CHAIN_APPROX_SIMPLE);
    for (auto &contour : contours)
    {
        int area = cv::contourArea(contour);
        if (area > largestArea)
        {
            largestArea = area;
            largestContour = contour;
        }
    }
    if (largestArea > 0)
    {
        cv::minEnclosingCircle(largestContour, ball_pos, ballRadius);
    }

    ballProcessingTime = processingTimeInterval.value();
}

void calibrateBallDetection(const cv::Mat frame)
{
    // Set the hue upper and lower bounds from the middle pixel of the image
    int hueChannel = 0;
    cv::Vec3b colour = frame.at<cv::Vec3b>(Point(utils::capture_width / 2, utils::capture_height / 2));

    // Picking thresholding values
    int hueMin = colour.val[hueChannel] - 5;
    int hueMax = colour.val[hueChannel] + 5;
    int saturationMin = colour.val[hueChannel + 1] - 5;
    int saturationMax = colour.val[hueChannel + 1] + 5;
    int valueMin = colour.val[hueChannel + 2] - 20;
    int valueMax = colour.val[hueChannel + 2] + 20;
    lowerBall_hsv = cv::Scalar(hueMin < 0 ? 0 : hueMin, saturationMin < 0 ? 0 : saturationMin, valueMin < 0 ? 0 : valueMin);
    upperBall_hsv = cv::Scalar(hueMax > 180 ? 180 : hueMax, saturationMax > 255 ? 255 : saturationMax, valueMax > 255 ? 255 : valueMax);

    // Little bit of debuging
    std::cout << "lowerBall_hsv is now: (" << std::to_string(hueMin) << ", " << std::to_string(saturationMin) << ", "
              << std::to_string(valueMin) << ")" << std::endl;
    std::cout << "upperBall_hsv is now: (" << std::to_string(hueMax) << ", " << std::to_string(saturationMax) << ", "
              << std::to_string(valueMax) << ")" << std::endl;
    return;
}

} // namespace ball

} // namespace detection
